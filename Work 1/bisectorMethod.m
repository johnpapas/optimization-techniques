%Optimization Techniques Project 1
%Author Papas Ioannis 9218
function [aV,bV,amin,bmin,timesCalled]=bisectorMethod(fun1,a,b,e,l)
%Simulates the bisector method for finding the minimum of a given function
%at a given section.
%e: is the step that we add and subtract to find in every iteration the 2
%points with which we will work
%l: the accuracy we want for the final result
%aV, bV: returned vectors with the edges of the section after every
%iteration
%amin,bmin: the final section in which the minimum is
%timesCalled: a number that indicates how many times the given function is
%called to calculate a value

timesCalled=0;
i=1;
aV(i)=a;
bV(i)=b;
while (b-a)>=l
    timesCalled=timesCalled+2;
    x1=((a+b)/2)-e;
    x2=((a+b)/2)+e;
    y1=fun1(x1);
    y2=fun1(x2);
    if(y1>y2)
        a=x1;
    else
        b=x2;
    end
    i=i+1;
    aV(i)=a;
    bV(i)=b;
    
end
amin=a;
bmin=b;


end