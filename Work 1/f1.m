%Optimization Techniques Project 1
%Author Papas Ioannis 9218
function y=f1(x)

y=((x-2).^2)-sin(x+3);

end
