%Optimization Techniques Project 1
%Author Papas Ioannis 9218
function y=f3(x)

y=(x.^2).*sin(x+2)-(x+1).^2;

end
