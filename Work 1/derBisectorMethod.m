%Optimization Techniques Project 1
%Author Papas Ioannis 9218
function [aV,bV,amin,bmin,i]=derBisectorMethod(fun1,a,b,l)
%Simulates the bisector method with the use of its derrivativefor finding the minimum of a given function
%at a given section.
%l: the accuracy we want for the final result
%aV, bV: returned vectors with the edges of the section after every
%iteration
%amin,bmin: the final section in which the minimum is
%timesCalled: a number that indicates how many times the given function is
%called to calculate a value

i=1;
aV(i)=a;
bV(i)=b;
xk=(a+b)/2;
%Here we create a new function which is the derrivative of our given
%function for every x. It will be used after for calculating the
%derrivative at a given point
syms x
f1 = eval(['@(x)' char(diff(fun1(x)))]);

while (b-a)>=l
    i=i+1;
    if(f1(xk)>0)
        b=xk;
    else
        a=xk;
    end
    aV(i)=a;
    bV(i)=b;
    xk=(a+b)/2;
end
amin=a;
bmin=b;

end