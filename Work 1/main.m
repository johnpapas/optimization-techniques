%Optimization Techniques Project 1
%Author Papas Ioannis 9218
%You should change manually the functions for which you want to run the
%algorithm
%%
%The section of the functions that we will be working
a=2;
b=5;
%%
%Bisector Method
%First question/Constant l
l=0.01;
e=0.0001:0.0001:0.004;
i=zeros(length(e),1);
minV=zeros(length(e),2);
for iT=1:length(e)
    [~,~,minV(iT,1),minV(iT,2),i(iT)]=bisectorMethod(@f1,a,b,e(iT),l);%change manually the function used for calculating
end
figure(1)
clf
plot(e,minV(:,1),e,minV(:,2));
title("Section in which the minimum is")
ylabel("x*")
xlabel("value of e")
legend("Minimum", "Maximum")
figure(2)
clf
plot(e,i)
title("Number of times the function is called for caluclating)")
xlabel("Value of e")
ylabel("# times f(x) is called")
%%
%Second question/Constant e
e=0.001;
l=0.005:0.005:0.5;
i=zeros(length(l),1);
minV=zeros(length(l),2);
for iT=1:length(l)
    [~,~,minV(iT,1),minV(iT,2),i(iT)]=bisectorMethod(@f1,a,b,e,l(iT));%change manually the function used for calculating
end
figure(3)
clf
plot(l,minV(:,1),l,minV(:,2));
title("Section in which the minimum is")
ylabel("x*")
xlabel("value of l")
legend("Minimum", "Maximum")
figure(4)
clf
plot(l,i)
title("Number of times the function is called for caluclating)")
xlabel("Value of l")
ylabel("# times f(x) is called")

%%
%Third question/Sections per iteration
e=0.001;
l=0.01;
[aV,bV,~,~,~]=bisectorMethod(@f1,a,b,e,l); %change manually the function used for calculating
figure(5)
clf
plot(aV)
hold on 
plot(bV)
title("Sections per iteration")
xlabel("# iteration")
ylabel("x of section")
legend("Minimum","Maximum")

%%
%Golden Ratio Method
%First question/Different accuracies
l=0.005:0.005:0.5;
i=zeros(length(l),1);
minV=zeros(length(l),2);
for iT=1:length(l)
    [~,~,minV(iT,1),minV(iT,2),i(iT)]=goldenRatioMethod(@f1,a,b,l(iT));%change manually the function used for calculating
end
figure(6)
clf
plot(l,minV(:,1),l,minV(:,2));
title("Section in which the minimum is")
ylabel("x*")
xlabel("value of l")
legend("Minimum", "Maximum")
figure(7)
clf
plot(l,i)
title("Number of times the function is called for caluclating)")
xlabel("Value of l")
ylabel("# times f(x) is called")

%%
%Second question/sections per iteration
l=0.01;
[aV,bV,~,~,~]=goldenRatioMethod(@f1,a,b,l); %change manually the function used for calculating
figure(8)
clf
plot(aV)
hold on 
plot(bV)
title("Sections per iteration")
xlabel("# iteration")
ylabel("x of section")
legend("Minimum","Maximum")

%%
%Fibonacci Method
%First question/Different accuracies
l=0.005:0.005:0.5;
i=zeros(length(l),1);
minV=zeros(length(l),2);
for iT=1:length(l)
    [~,~,minV(iT,1),minV(iT,2),i(iT)]=fibonacciMethod(@f1,a,b,l(iT));%change manually the function used for calculating
end
figure(9)
clf
plot(l,minV(:,1),l,minV(:,2));
title("Section in which the minimum is")
ylabel("x*")
xlabel("value of l")
legend("Minimum", "Maximum")
figure(10)
clf
plot(l,i)
title("Number of times the function is called for caluclating)")
xlabel("Value of l")
ylabel("# times f(x) is called")

%%
%Second question/sections per iteration
l=0.01;
[aV,bV,~,~,~]=fibonacciMethod(@f1,a,b,l); %change manually the function used for calculating
figure(11)
clf
plot(aV)
hold on 
plot(bV)
title("Sections per iteration")
xlabel("# iteration")
ylabel("x of section")
legend("Minimum","Maximum")

%%
%Bisector with derrivative Method
%First question/Different accuracies
l=0.005:0.005:0.5;
i=zeros(length(l),1);
minV=zeros(length(l),2);
for iT=1:length(l)
    [~,~,minV(iT,1),minV(iT,2),i(iT)]=derBisectorMethod(@f1,a,b,l(iT));%change manually the function used for calculating
end
figure(12)
clf
plot(l,minV(:,1),l,minV(:,2));
title("Section in which the minimum is")
ylabel("x*")
xlabel("value of l")
legend("Minimum", "Maximum")
figure(13)
clf
plot(l,i)
title("Number of times the function is called for caluclating)")
xlabel("Value of l")
ylabel("# times f(x) is called")

%%
%Second question/sections per iteration
l=0.01;
[aV,bV,~,~,~]=derBisectorMethod(@f1,a,b,l); %change manually the function used for calculating
figure(14)
clf
plot(aV)
hold on 
plot(bV)
title("Sections per iteration")
xlabel("# iteration")
ylabel("x of section")
legend("Minimum","Maximum")

%%
%Results
l=0.01;
e=0.001;
x=2:0.0001:5;
minV=zeros(4,2);
[~,~,minV(1,1),minV(1,2),~]=bisectorMethod(@f1,a,b,e,l);
[~,~,minV(2,1),minV(2,2),~]=goldenRatioMethod(@f1,a,b,l);
[~,~,minV(3,1),minV(3,2),~]=fibonacciMethod(@f1,a,b,l);
[~,~,minV(4,1),minV(4,2),~]=derBisectorMethod(@f1,a,b,l);
figure(15)
clf
plot(x,f1(x))
hold on
y=[min(f1(x)),max(f1(x))];
plot([minV(1,1) minV(1,1)],y,"r",[minV(2,1) minV(2,1)],y,"g",[minV(3,1) minV(3,1)],y,"b",[minV(4,1) minV(4,1)],y, "k")
hold on
plot([minV(1,2) minV(1,2)],y,"r",[minV(2,2) minV(2,2)],y,"g",[minV(3,2) minV(3,2)],y,"b",[minV(4,2) minV(4,2)],y, "k")
% legend("f(x)", "Bisector Method", "Golden Ratio Method", "Fibonacci Method", "Derrivative Bisector Method")
title("Results of all")









