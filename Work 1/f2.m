%Optimization Techniques Project 1
%Author Papas Ioannis 9218
function y=f2(x)

y=exp(-5*x)+(x+2).*(cos(0.5*x).^2);

end
