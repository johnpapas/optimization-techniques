%Optimization Techniques Project 1
%Author Papas Ioannis 9218
function [aV,bV,amin,bmin,timesCalled]=fibonacciMethod(fun1,a,b,l)
%Simulates the fibonacci method for finding the minimum of a given function
%at a given section.
%l: the accuracy we want for the final result
%aV, bV: returned vectors with the edges of the section after every
%iteration
%amin,bmin: the final section in which the minimum is
%timesCalled: a number that indicates how many times the given function is
%called to calculate a value

timesCalled=0;
i=1;
aV(i)=a;
bV(i)=b;
%Here we find the number of iterations and the n-th fibonacci value used for our algorithm 
vector=1:25;
nV=(fibonacci(vector))>((b-a)/l);
n=find(nV,1,'first');
x1=a+(fibonacci(n-i-1)/fibonacci(n-i+1))*(b-a);
x2=a+(fibonacci(n-i)/fibonacci(n-i+1))*(b-a);

while (b-a)>=l
    timesCalled=timesCalled+2;
    i=i+1;
    y1=fun1(x1);
    y2=fun1(x2);
    if(y1>y2)
        a=x1;
        x1=x2;
        x2=a+(fibonacci(n-i)/fibonacci(n-i+1))*(b-a);
    else
        b=x2;
        x2=x1;
        x1=a+(fibonacci(n-i-1)/fibonacci(n-i+1))*(b-a);
    end
    aV(i)=a;
    bV(i)=b;
end
amin=a;
bmin=b;

end