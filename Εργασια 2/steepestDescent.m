%Optimization Techniques Project 2
%Author Papas Ioannis 9218

%%
%Variables
h=0.1;
[X,Y] = meshgrid(-3:h:3,-1.5:(h/2):1.5);
f=function1(X,Y);
%%
%First question/our function plot
figure(1)%3d plot
clf
surf(X,Y,f)
title("The function for which we will be working")
xlabel("X")
ylabel("Y")
zlabel("f")
[px,py]=gradient(f);
figure(2) %Quiver contour plot
clf
contour(X,Y,f);
hold on
quiver(X,Y,px,py)
xlabel("X")
ylabel("Y")
title("The contour lines and vectors")

%%
%Steepest descent method/constant gamma
e=0.01;
xk=(-1)*ones(2,1);
xM=xk;
gamma=0.1;
k=1;
syms X Y
Z=function1(X,Y);
dX = matlabFunction(diff(Z,X));
dY = matlabFunction(diff(Z,Y));
gradf=[dX(xk(1),xk(2)) dY(xk(1),xk(2))];
dk=-gradf;
while (norm(gradf)>e)
    k=k+1;
    xk=xk+gamma*dk';
    xM(:,k)=xk;
    gradf=[dX(xk(1),xk(2)) dY(xk(1),xk(2))];
    dk=-gradf;
end
[X,Y] = meshgrid(-3:h:3,-1.5:(h/2):1.5);
f=function1(X,Y);
figure(3) %Quiver contour plot
clf
contour(X,Y,f);
hold on
quiver(X,Y,px,py)
hold on
plot(xM(1,:),xM(2,:),"-")
hold on
plot(xM(1,1),xM(2,1),"o")
hold on
plot(xM(1,k),xM(2,k),"o")
xlabel("X")
ylabel("Y")
title("The path from the initial until the final point on the quiver plot")
fprintf("The number of iterations needed is %d\n", k)
fprintf("The minimum of f(x,y) is [%f %f]\n", xM(1,k), xM(2,k))

%%
%Steepest descent method/miniminzing f(xk-g*grad(f(xk))
e=0.001;
k=1;
xk=(-1)*ones(2,1);
xM=xk;
gammaV=0;
syms X Y
Z=function1(X,Y);
dX = matlabFunction(diff(Z,X));
dY = matlabFunction(diff(Z,Y));
gradf=[dX(xk(1),xk(2)) dY(xk(1),xk(2))];
dk=-gradf;
while (norm(gradf)>e)
    syms gamma
    init2=xk+gamma*dk';
    Z1=matlabFunction(function1(init2(1),init2(2)));
    g=fminbnd(Z1,0,5);
    gammaV(k)=g;
    k=k+1;
    xk=xk+g*dk';
    xM(:,k)=xk;
    gradf=[dX(xk(1),xk(2)) dY(xk(1),xk(2))];
    dk=-gradf;
end
[X,Y] = meshgrid(-3:h:3,-1.5:(h/2):1.5);
f=function1(X,Y);
figure(4) %Quiver contour plot
clf
contour(X,Y,f);
hold on
quiver(X,Y,px,py)
hold on
plot(xM(1,:),xM(2,:),"-")
hold on
plot(xM(1,1),xM(2,1),"o")
hold on
plot(xM(1,k),xM(2,k),"o")
xlabel("X")
ylabel("Y")
title("The path from the initial until the final point on the quiver plot")
fprintf("The number of iterations needed is %d\n", k)
fprintf("The minimum of f(x,y) is [%f %f]\n", xM(1,k), xM(2,k))
figure(5)
clf
plot(1:(k-1),gammaV,"-o")
title("The gamma we choose in each iteration")
xlabel("# of iteration")
ylabel("Value of �")

%%
%Steepest descent method/Armijo method
e=0.001;
k=1;
gV=0;
s=0.7;
a=10^-2;
b=0.2;
mk=0:1:30;
xk=(-1)*ones(2,1);
xM=xk;
syms X Y
Z=function1(X,Y);
dX = matlabFunction(diff(Z,X));
dY = matlabFunction(diff(Z,Y));
gradf=[dX(xk(1),xk(2)) dY(xk(1),xk(2))];
dk=-gradf;
gammaV=s*(b.^mk);
while (norm(-dk)>e)
    xnext=xk'+gammaV'*dk;
    gammaFunction=function1(xk(1),xk(2))-function1(xnext(:,1),xnext(:,2))-a*gammaV'*(dk*dk');
    gamma=gammaV(find((gammaFunction>=0),1,'first'));
    gV(k)=gamma;
    k=k+1;
    xk=xk+gamma*dk';
    xM(:,k)=xk;
    gradf=[dX(xk(1),xk(2)) dY(xk(1),xk(2))];
    dk=-gradf;
end
[X,Y] = meshgrid(-3:h:3,-1.5:(h/2):1.5);
f=function1(X,Y);
figure(6) %Quiver contour plot
clf
contour(X,Y,f);
hold on
quiver(X,Y,px,py)
hold on
plot(xM(1,:),xM(2,:),"-")
hold on
plot(xM(1,1),xM(2,1),"o")
hold on
plot(xM(1,k),xM(2,k),"o")
xlabel("X")
ylabel("Y")
title("The path from the initial until the final point on the quiver plot")
fprintf("The number of iterations needed is %d\n", k)
fprintf("The minimum of f(x,y) is [%f %f]\n", xM(1,k), xM(2,k))
figure(7)
clf
plot(1:(k-1),gV,"-o")
title("The gamma we choose in each iteration")
xlabel("# of iteration")
ylabel("Value of �")

