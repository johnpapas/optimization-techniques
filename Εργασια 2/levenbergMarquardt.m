%Optimization Techniques Project 2
%Author Papas Ioannis 9218

%%
%Levenberg-Marquardt Method/Constant gamma
e=0.001;
xk=(1)*ones(2,1);
xM=xk;
gamma=1;
k=1;
syms X Y
Z=function1(X,Y);
dX = matlabFunction(diff(Z,X));
dY = matlabFunction(diff(Z,Y));
gradf=[dX(xk(1),xk(2)) dY(xk(1),xk(2))];
dX2=matlabFunction(diff(dX,X));
dY2=matlabFunction(diff(dY,Y));
dXY=matlabFunction(diff(dX,Y));
dYX=matlabFunction(diff(dY,X));
while (norm(gradf)>e)
    k=k+1;
    hessian=[dX2(xk(1),xk(2)) dYX(xk(1),xk(2));dXY(xk(1),xk(2)) dY2(xk(1),xk(2))];
    syms mk
    matrix=hessian+mk*eye(2);
    eigenvalues=eig(matrix);
    S1=solve(eigenvalues(1)==0,mk);
    S2=solve(eigenvalues(2)==0,mk);
    solutions=double([S1 S2]);
    mkReal=max(solutions(solutions>0))+1;
    if(isempty(mkReal))
        mkReal=0;
    end
    dk=-((hessian+mkReal*eye(2))^-1)*gradf';
    xk=xk+gamma*dk;
    xM(:,k)=xk;
    gradf=[dX(xk(1),xk(2)) dY(xk(1),xk(2))];
end
h=0.1;
[X,Y] = meshgrid(-3:h:3,-1.5:(h/2):1.5);
f=function1(X,Y);
[px,py]=gradient(f);
figure(3) %Quiver contour plot
clf
contour(X,Y,f);
hold on
quiver(X,Y,px,py)
hold on
plot(xM(1,:),xM(2,:),"-")
hold on
plot(xM(1,1),xM(2,1),"o")
hold on
plot(xM(1,k),xM(2,k),"o")
xlabel("X")
ylabel("Y")
title("The path from the initial until the final point on the quiver plot")
fprintf("The number of iterations needed is %d\n", k)
fprintf("The minimum of f(x,y) is [%f %f]\n", xM(1,k), xM(2,k))

%%
%Levenberg-Marquardt Method/miniminzing f(xk+g*dk)
e=0.001;
xk=(1)*ones(2,1);
xM=xk;
gammaV=0;
k=1;
syms X Y
Z=function1(X,Y);
dX = matlabFunction(diff(Z,X));
dY = matlabFunction(diff(Z,Y));
gradf=[dX(xk(1),xk(2)) dY(xk(1),xk(2))];
dX2=matlabFunction(diff(dX,X));
dY2=matlabFunction(diff(dY,Y));
dXY=matlabFunction(diff(dX,Y));
dYX=matlabFunction(diff(dY,X));
while (norm(gradf)>e)
    k=k+1;
    hessian=[dX2(xk(1),xk(2)) dYX(xk(1),xk(2));dXY(xk(1),xk(2)) dY2(xk(1),xk(2))];
    syms mk
    matrix=hessian+mk*eye(2);
    eigenvalues=eig(matrix);
    S1=solve(eigenvalues(1)==0,mk);
    S2=solve(eigenvalues(2)==0,mk);
    solutions=double([S1 S2]);
    mkReal=min(solutions(solutions>0))+1;
    if(isempty(mkReal))
        mkReal=0;
    end
    dk=-((hessian+mkReal*eye(2))^-1)*gradf';
    syms gamma
    init2=xk+gamma*dk;
    Z1=matlabFunction(function1(init2(1),init2(2)));
    g=fminbnd(Z1,0,5);
    gammaV(k-1)=g;
    xk=xk+g*dk;
    xM(:,k)=xk;
    gradf=[dX(xk(1),xk(2)) dY(xk(1),xk(2))];
end
h=0.1;
[X,Y] = meshgrid(-3:h:3,-1.5:(h/2):1.5);
f=function1(X,Y);
[px,py]=gradient(f);
figure(3) %Quiver contour plot
clf
contour(X,Y,f);
hold on
quiver(X,Y,px,py)
hold on
plot(xM(1,:),xM(2,:),"-")
hold on
plot(xM(1,1),xM(2,1),"o")
hold on
plot(xM(1,k),xM(2,k),"o")
xlabel("X")
ylabel("Y")
title("The path from the initial until the final point on the quiver plot")
fprintf("The number of iterations needed is %d\n", k)
fprintf("The minimum of f(x,y) is [%f %f]\n", xM(1,k), xM(2,k))
figure(5)
clf
plot(1:(k-1),gammaV,"-o")
title("The gamma we choose in each iteration")
xlabel("# of iteration")
ylabel("Value of �")

%%
%Levenberg-Marquardt Method/Armijo Method
e=0.001;
s=4;
a=10^-2;
b=0.2;
mk=0:1:30;
gammaV=s*(b.^mk);
xk=(-1)*ones(2,1);
xM=xk;
gV=0;
k=1;
syms X Y
Z=function1(X,Y);
dX = matlabFunction(diff(Z,X));
dY = matlabFunction(diff(Z,Y));
gradf=[dX(xk(1),xk(2)) dY(xk(1),xk(2))];
dX2=matlabFunction(diff(dX,X));
dY2=matlabFunction(diff(dY,Y));
dXY=matlabFunction(diff(dX,Y));
dYX=matlabFunction(diff(dY,X));
while (norm(gradf)>e)
    k=k+1;
    hessian=[dX2(xk(1),xk(2)) dYX(xk(1),xk(2));dXY(xk(1),xk(2)) dY2(xk(1),xk(2))];
    syms mk
    matrix=hessian+mk*eye(2);
    eigenvalues=eig(matrix);
    S1=solve(eigenvalues(1)==0,mk);
    S2=solve(eigenvalues(2)==0,mk);
    solutions=double([S1 S2]);
    mkReal=min(solutions(solutions>0))+1;
    if(isempty(mkReal))
        mkReal=0;
    end
    dk=-((hessian+mkReal*eye(2))^-1)*gradf';
    xnext=xk'+gammaV'*dk';
    gammaFunction=function1(xk(1),xk(2))-function1(xnext(:,1),xnext(:,2))+a*gammaV'*(dk'*gradf');
    gamma=gammaV(find((gammaFunction>=0),1,'first'));
    gV(k-1)=gamma;
    xk=xk+gamma*dk;
    xM(:,k)=xk;
    gradf=[dX(xk(1),xk(2)) dY(xk(1),xk(2))];
end
h=0.1;
[X,Y] = meshgrid(-3:h:3,-1.5:(h/2):1.5);
f=function1(X,Y);
[px,py]=gradient(f);
figure(3) %Quiver contour plot
clf
contour(X,Y,f);
hold on
quiver(X,Y,px,py)
hold on
plot(xM(1,:),xM(2,:),"-")
hold on
plot(xM(1,1),xM(2,1),"o")
hold on
plot(xM(1,k),xM(2,k),"o")
xlabel("X")
ylabel("Y")
title("The path from the initial until the final point on the quiver plot")
fprintf("The number of iterations needed is %d\n", k)
fprintf("The minimum of f(x,y) is [%f %f]\n", xM(1,k), xM(2,k))
figure(5)
clf
plot(1:(k-1),gV,"-o")
title("The gamma we choose in each iteration")
xlabel("# of iteration")
ylabel("Value of �")


