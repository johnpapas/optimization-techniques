%Optimization Techniques Project
%Author Ioannis Papas 9218

population_size=100;
generation=1;
population=zeros(population_size,7); %First six are the parameters, 7th the FitFunScore

%%
%Initialize
for i=1:population_size
    kDP=1+99*rand(4,1);
    population(i,1:4)=kDP;
    kI=1+9*rand(2,1);
    population(i,5:6)=kI;
    population(i,7)=fitnessFunction(kDP(1),kDP(2),kDP(3),kDP(4),kI(1),kI(2));
end

[~,idx]=sort(population(:,7));
population=population(idx,:);

%%
%Creating new gens
fscores(generation)=population(1,7);
while (population(1,7)>=40) %Change ending criteria here
    %selection
    newGen=zeros(population_size,7);
    newGen(1:10,:)=population(1:10,:);
    r1 = randi([1 50],1,90);
    r2 = randi([1 50],1,90);
    parents1=population(r1,:);
    parents2=population(r2,:);
    %mutation and crossover
    childs=mate(parents1, parents2);
    newGen(11:population_size,:)=childs;
    generation=generation+1;
    population=newGen;
    [~,idx]=sort(population(:,7));
    population=population(idx,:);
    fscores(generation)=population(1,7);
    if (generation>50)
        break;
    end
end

%%
%plotting
tf=50;
tspan=0:0.1:tf;
[x,t,u] = simclosedloop(population(1,1),population(1,2),population(1,5),population(1,6),population(1,3),population(1,4),tf);
figure(1)
plot(t,u(:,1),t,u(:,2))
title("Controller input")
xlabel("Time(s)")
ylabel("Value of input")
legend("u1","u2")
yd1=pi/2+(pi/6)*cos(tspan);
yd2=pi/2+(pi/6)*sin(tspan);

figure(2)
plot(t,x(:,1)-yd1',t,x(:,3)-yd2')
title("Error plot")
xlabel("Time(s)")
ylabel("Error")
legend("e1","e2")

figure(3)
plot(fscores)
title("The fitness function scores for each gen")
xlabel("# generation")
ylabel("Value of FF")





