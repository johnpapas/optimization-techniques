%Optimization Techniques Project
%Author Ioannis Papas 9218

function child=mate(parent1, parent2)
%A function that mates two parents for the creation of a child.

nParents=length(parent1(:,1));
nGenes=length(parent1(1,:))-1;
child=zeros(nParents,nGenes+1);

for i=1:nParents
    %Each gene is selected with 45% from the first parent, 45% from the
    %second and 10% as a mutation, that creates a new gene/value
    for j=1:nGenes
        p1=parent1(i,:);
        p2=parent2(i,:);
        p=rand(1);
        %crossover
        if(p<0.45)
            child(i,j)=p1(j);
        elseif (p>=0.45 && p<0.9)
            child(i,j)=p2(j);
        %mutation
        elseif (p>=0.9 && j<=4) %check if not integral parameter
            child(i,j)=1+99*rand(1);
        elseif (p>=0.9 && j>4)
            child(i,j)=1+9*rand(1);
        end
    end
    child(i,7)=fitnessFunction(child(i,1),child(i,2),child(i,3),child(i,4),child(i,5),child(i,6));
end

end
