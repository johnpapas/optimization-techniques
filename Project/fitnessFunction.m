%Optimization Techniques Project
%Author Ioannis Papas 9218

function fitnessFun=fitnessFunction(kp1,kp2,kd1,kd2,ki1,ki2)
%A function that calculates the value of our fitness function given the 6
%parameters of the controller

tf=50;
[x,t,u] = simclosedloop(kp1,kp2,ki1,ki2,kd1,kd2,tf);
tspan=0:0.1:tf;
nSamples=length(tspan);

yd1=pi/2+(pi/6)*cos(tspan);
yd2=pi/2+(pi/6)*sin(tspan);

e=[x(:,1)-yd1' x(:,3)-yd2'];

bothInZone=(e(:,1)<=pi/180) & (e(:,1)>=-pi/180) & (e(:,2)<=pi/180) & (e(:,2)>=-pi/180);
ns=find(bothInZone,1);

if(isempty(ns) || abs(max(max(e(ns:nSamples))))>pi/180)
    J1=50*pi/1500;
else
    J1=abs(max(max(e(ns:nSamples))));
end

eachInZone=[((e(:,1)<=pi/180) & (e(:,1)>=-pi/180)) ((e(:,2)<=pi/180) & (e(:,2)>=-pi/180))];
ni=[find(eachInZone(:,1),1) find(eachInZone(:,2),1)];

if(length(ni)~=2)
    J2=50*pi/1500;
elseif(isempty(ns))
    J2=abs(max(max([e(ni(1):nSamples) e(ni(2):nSamples)])));
    if(J2>=pi/180)
        J2=50*pi/1500;
    end
else
    J2=abs(max(max([e(ni(1):ns) e(ni(2):ns)])));
end

if((isempty(ns)) || (ns>=10))
    J3=60;
else
    J3=ns;
end

if max(max(abs(u)))>=18
    J4=108;
else
    J4=max(max(abs(u)));
end

if max(max(abs(u(2:nSamples)-u(1:nSamples-1))))>=16
    J5=96;
else
    J5=max(max(abs(u(2:nSamples)-u(1:nSamples-1))));
end

J6=max(max(u)-min(u));

fitnessFun=1500*J1/pi+1500*J2/pi+5*J3/6+50*J4/108+50*J5/96+50*J6/(6*(2*max(max(abs(u)))));



end
