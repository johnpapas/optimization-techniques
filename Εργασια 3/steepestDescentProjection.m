%Optimization Techniques Project 3
%Author Papas Ioannis 9218

%%
%Steepest descent method/constant gamma
e=0.01;
xk=[11;3]; %Change the initial position manually
xM=xk;
gamma=0.01;
s=0.1;
k=1;
syms X Y
Z=function1(X,Y);
dX = matlabFunction(diff(Z,X));
dY = matlabFunction(diff(Z,Y));
gradf=[dX(xk(1)) dY(xk(2))];
while (norm(gradf)>e)
    k=k+1;
    if (xk(1)<=-20 && xk(2)<=-12)
         xk=xk+gamma*([-20;-12]-xk);
    elseif (xk(1)<=-20 && xk(2)>=15)
        xk=xk+gamma*([-20;15]-xk);
    elseif (xk(1)>=10 && xk(2)<=-12)
        xk=xk+gamma*([10;-12]-xk);
    elseif (xk(1)>=10 && xk(2)>=15)
        xk=xk+gamma*([10;15]-xk);
    elseif (xk(1)>-20 && xk(1)<10 && xk(2)<=-12)
        xk=xk+gamma*([(1-s)*xk(1);-12]-xk);    
    elseif (xk(1)>-20 && xk(1)<10 && xk(2)>=15)
        xk=xk+gamma*([(1-s)*xk(1);15]-xk);  
    elseif (xk(2)>-12 && xk(2)<15 && xk(1)<=-20)
        xk=xk+gamma*([-20;(1-s)*xk(2)]-xk);    
    elseif (xk(2)>-12 && xk(2)<15 && xk(1)>=10)
        xk=xk+gamma*([10;(1-s)*xk(2)]-xk);
    else
        xk=(1-s*gamma)*xk;
    end
    xM(:,k)=xk;
    gradf=[dX(xk(1)) dY(xk(2))];
    if (k>10000)
        break;
    end
end
h=0.1;
[X,Y] = meshgrid(-20:h:20,-20:h:20);
f=function1(X,Y);
figure(1) 
clf
contour(X,Y,f);
hold on
plot(xM(1,:),xM(2,:),"-")
hold on
plot(xM(1,1),xM(2,1),"o")
hold on
plot(xM(1,k),xM(2,k),"o")
xlabel("X1")
ylabel("X2")
title("The path from the initial until the final point on the quiver plot")
fprintf("The number of iterations needed is %d\n", k)
fprintf("The minimum of f(x,y) is [%f %f]\n", xM(1,k), xM(2,k))
figure(2) 
plot(1:k,xM(1,:),1:k,xM(2,:))
xlabel("# iteration")
legend("xk1","xk2")
title("The value of xk in each iteration")