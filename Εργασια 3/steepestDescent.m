%Optimization Techniques Project 3
%Author Papas Ioannis 9218

%%
%Variables
h=0.1;
[X,Y] = meshgrid(-3:h:3,-3:h:3);
f=function1(X,Y);
%%
%First question/our function plot
figure(1)%3d plot
clf
surf(X,Y,f)
title("The function for which we will be working")
xlabel("X1")
ylabel("�2")
zlabel("f")
[px,py]=gradient(f);
figure(2) %Quiver contour plot
clf
contour(X,Y,f);
hold on
quiver(X,Y,px,py)
xlabel("X1")
ylabel("�2")
title("The contour lines and vectors")

%%
%Steepest descent method/constant gamma
e=0.01;
xk=(1)*ones(2,1);
xM=xk;
gamma=0.1;
k=1;
syms X Y
Z=function1(X,Y);
dX = matlabFunction(diff(Z,X));
dY = matlabFunction(diff(Z,Y));
gradf=[dX(xk(1)) dY(xk(2))];
dk=-gradf;
while (norm(gradf)>e)
    k=k+1;
    xk=xk+gamma*dk';
    xM(:,k)=xk;
    gradf=[dX(xk(1)) dY(xk(2))];
    dk=-gradf;
end
[X,Y] = meshgrid(-3:h:3,-3:h:3);
f=function1(X,Y);
figure(3) %Quiver contour plot
clf
contour(X,Y,f);
hold on
quiver(X,Y,px,py)
hold on
plot(xM(1,:),xM(2,:),"-")
hold on
plot(xM(1,1),xM(2,1),"o")
hold on
plot(xM(1,k),xM(2,k),"o")
xlabel("X1")
ylabel("X2")
title("The path from the initial until the final point on the quiver plot")
fprintf("The number of iterations needed is %d\n", k)
fprintf("The minimum of f(x,y) is [%f %f]\n", xM(1,k), xM(2,k))



